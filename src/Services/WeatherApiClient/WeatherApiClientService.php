<?php


namespace App\Services\WeatherApiClient;


use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WeatherApiClientService implements WeatherApiClientServiceInterface
{

    private const PARAMS_QUERY = 'query';


    public function __construct(
        private HttpClientInterface $weatherClient,
        private LoggerInterface $logger
    )
    {
    }


    public function get(string $url, array $params): array
    {
        $result      = [];
        $queryParams = [];

        if (!empty($params)) {
            $queryParams[self::PARAMS_QUERY] = $params;
        }

        try {
            $response = $this->weatherClient->request('GET', $url, $queryParams);
            $result   = $response->toArray();
        }

        catch (Exception $exception) {

            $this->logger->critical(
                sprintf(
                    '%s: Unable to fetch from Weather api for resource: %s message: %s: ',
                    __METHOD__,
                    $url,
                    $exception->getMessage()
                )
            );
        }

        return $result;
    }
}