<?php


namespace App\Services\WeatherApiClient;


interface WeatherApiClientServiceInterface
{
    /**
     * @param string $url
     * @param array  $params
     *
     * @return array
     */
    public function get(string $url, array $params): array;

}